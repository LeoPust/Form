var     express         =       require(    'express'       ),
    app             =       express(),
    logger          =       require(    'morgan'        ),
    bodyParser      =       require(    'body-parser'   ),
    cookieParser    =       require(    'cookie-parser' ),
    jade            =       require(    'jade'          ),
    Routes          =       require(    './Routes'      ),
    orm             =       require(    "orm"           ),
    transaction     =       require(    "orm-transaction"),
    DBShema         =       require(    "./DBModel"     );
app.use(    logger('dev')      );
app.use(bodyParser.urlencoded({ extended: true }));
app.use(    cookieParser()          );
app.set('trust proxy');
app.set(    'view engine', 'jade'                               );
app.use(    express.static('file')  );
app.use(    express.static('file/other')  );
app.use(    '/file/js',             express.static('js')        );
app.use(    '/file/css',            express.static('css')       );
app.use(    '/file/other',          express.static('file')      );
app.use(    '/file/img',            express.static('img')       );

app.use(orm.express("mysql://root:@localhost/db_questions", {
    define: function (db, models, next) {
        for(var i in DBShema(db)){
            models[i] = DBShema(db)[i];
        }
        db.use(transaction);
        next();
    }
}));

app.route(Routes.route)
    .all(Routes);

app.route("*")
    .all(function(req,res){
        res.sendStatus(404);
    });

app.listen(9000);
console.log("Server start!");