var controller = {
    get resSend(){
        return require("./resSend/index");
    },
    get AddFeedBack(){
        return require("./AddFeedBack/index");
    },
    get GetAllFeedBack(){
        return require("./GetAllFeedBack/index");
    },
    get models(){
        return require("./models/index");
    },
    get validateBody(){
        return require("./validateBody/index");
    },
    get allSum(){
        return require("./allSum/index");
    },
    get UpdateUser(){
        return require("./UpdateUser/index");
    }
};

module.exports = function(value){
    if([value] in controller){
        return controller[value];
    }else{
        return null;
    }
};