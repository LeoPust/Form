module.exports = function(req,callback){
    if(req.method == "GET"){
      if(req.query.login == 'admin' && req.query.pass == 'admin'){
          req.db.driver.execQuery(
              "select r.date as date,r.id as id,r.value as value,r.name as name,r.sum as sum,q1.q_a as q_1_a,q1.q_b as q_1_b,q1.q_c as q_1_c,q2.q_a as q_2_a,q2.q_b as q_2_b,q2.q_c as q_2_c,q3.value as q_3,q4.value as q_4" +
              " from route as r left join question_1 as q1 on q1.routeId = r.id left join question_2 as q2 on q2.routeId = r.id " +
              "left join question_3 as q3 on q3.routeId = r.id left join question_4 as q4 on q4.routeId = r.id ORDER by r.date DESC"
              ,function(err,data){
              !err ? callback({status:200,render:'out',body:{arr:data}})
                  :callback({status:403});
          });
      }
  }else{
        if(req.body.limit_1 && req.body.limit_2 && req.body.q && req.body.sort){
            if(req.body.limit_1 == 0 && req.body.limit_2 == 0){
                req.db.driver.execQuery(
                    "select r.date as date,r.id as id,r.value as value,r.name as name,r.sum as sum,q1.q_a as q_1_a,q1.q_b as q_1_b,q1.q_c as q_1_c,q2.q_a as q_2_a,q2.q_b as q_2_b,q2.q_c as q_2_c,q3.value as q_3,q4.value as q_4" +
                    " from route as r left join question_1 as q1 on q1.routeId = r.id left join question_2 as q2 on q2.routeId = r.id " +
                    "left join question_3 as q3 on q3.routeId = r.id left join question_4 as q4 on q4.routeId = r.id ORDER by "+req.body.q+" "+req.body.sort
                    ,function(err,data){
                        !err ? callback({status:200,render:'out',body:{arr:data,q:req.body.q,sort:req.body.sort}})
                            :callback({status:403});
                    });
            }else{
                var min,max;
                if(req.body.limit_2 > req.body.limit_1){
                    min = req.body.limit_1;
                    max = req.body.limit_2;
                }else{
                    min = req.body.limit_2;
                    max = req.body.limit_1;
                }
                req.db.driver.execQuery(
                    "select r.date as date,r.id as id,r.value as value,r.name as name,r.sum as sum,q1.q_a as q_1_a,q1.q_b as q_1_b,q1.q_c as q_1_c,q2.q_a as q_2_a,q2.q_b as q_2_b,q2.q_c as q_2_c,q3.value as q_3,q4.value as q_4" +
                    " from route as r left join question_1 as q1 on q1.routeId = r.id left join question_2 as q2 on q2.routeId = r.id " +
                    "left join question_3 as q3 on q3.routeId = r.id left join question_4 as q4 on q4.routeId = r.id where "+req.body.q +" >= "+min+" and "+req.body.q +" <= "+max+" ORDER by "+req.body.q+" "+req.body.sort
                    ,function(err,data){
                        !err ? callback({status:200,render:'out',body:{arr:data,q:req.body.q,sort:req.body.sort,min:min,max:max}})
                            :callback({status:403});
                    });
            }
        }else{
            var key = Object.keys(req.body);
            if(/^(name)/.test(key[0])){
                req.models.route.find({id:parseInt(key[0].split("__")[1])},function(err,route){
                    console.log(route);
                    if(!err){
                        route[0].name = req.body[key[0]];
                        route[0].save(function(err){
                            !err ? req.db.driver.execQuery("update route set name = ? where value = ?",[req.body[key[0]],route[0].value],function(err){
                                !err ? callback({status:200,redirect:"/admin/?login=admin&pass=admin"})
                                    : callback({status:403});
                            })
                                :callback({status:403});
                        })
                    }else{
                        callback({status:403});
                    }
                });
            }else{
                callback({status:403});
            }

        }
    }
};
