module.exports = function(body){
  var sum = 0;
    var arrObj = {name:[
        'question_1_a',
        'question_1_b',
        'question_1_c',
        'question_2_a',
        'question_2_b',
        'question_2_c',
        'question_3',
        'question_4'
    ],value:[
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ]};
    for(var i in body){
        if(arrObj.name.indexOf(i) != -1){
            arrObj.value[arrObj.name.indexOf(i)] = parseInt(body[i]);
        }
    }
    for(var i in arrObj.value){
        sum += arrObj.value[i];
    }
    return sum;
};