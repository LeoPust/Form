var Components = require("./components/index");

var route = [
    '/',
    '/admin/?',
    '/upd',
    '/*?'
];



module.exports = function(req,res){
    Components("models")(req,function(result){
        Components("resSend")(res,result);
    });
};

module.exports.route = route;