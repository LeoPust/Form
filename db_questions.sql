-- MySQL dump 10.13  Distrib 5.6.26, for osx10.10 (x86_64)
--
-- Host: localhost    Database: db_questions
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `question_1`
--

DROP TABLE IF EXISTS `question_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_1` (
  `routeId` int(200) NOT NULL,
  `q_a` int(200) NOT NULL,
  `q_b` int(200) NOT NULL,
  `q_c` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_1`
--

LOCK TABLES `question_1` WRITE;
/*!40000 ALTER TABLE `question_1` DISABLE KEYS */;
INSERT INTO `question_1` VALUES (23,5,3,8),(24,10,10,10),(25,10,10,10),(26,1,1,1),(27,1,1,1),(28,8,8,8),(29,10,1,1),(30,10,1,1),(31,10,1,1),(32,10,1,1),(33,10,10,10),(34,2,2,2),(35,6,6,6),(36,2,8,2),(37,4,4,8),(38,1,2,3),(39,1,1,10),(40,1,1,1),(41,6,7,4),(42,1,1,1),(43,5,6,8);
/*!40000 ALTER TABLE `question_1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_2`
--

DROP TABLE IF EXISTS `question_2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_2` (
  `routeId` int(200) NOT NULL,
  `q_a` int(200) NOT NULL,
  `q_b` int(200) NOT NULL,
  `q_c` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_2`
--

LOCK TABLES `question_2` WRITE;
/*!40000 ALTER TABLE `question_2` DISABLE KEYS */;
INSERT INTO `question_2` VALUES (23,5,3,8),(24,10,10,10),(25,10,10,10),(26,10,1,1),(27,1,1,1),(28,8,8,8),(29,1,1,1),(30,1,1,1),(31,1,1,1),(32,1,1,1),(33,1,1,1),(34,4,7,9),(35,6,6,1),(36,2,7,4),(37,6,4,8),(38,4,5,6),(39,10,10,10),(40,1,1,1),(41,9,2,4),(42,1,4,5),(43,5,5,6);
/*!40000 ALTER TABLE `question_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_3`
--

DROP TABLE IF EXISTS `question_3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_3` (
  `routeId` int(200) NOT NULL,
  `value` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_3`
--

LOCK TABLES `question_3` WRITE;
/*!40000 ALTER TABLE `question_3` DISABLE KEYS */;
INSERT INTO `question_3` VALUES (23,2),(24,10),(25,10),(26,5),(27,1),(28,8),(29,10),(30,10),(31,10),(32,10),(33,10),(34,7),(35,10),(36,7),(37,3),(38,7),(39,4),(40,1),(41,4),(42,7),(43,6);
/*!40000 ALTER TABLE `question_3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_4`
--

DROP TABLE IF EXISTS `question_4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_4` (
  `routeId` int(200) NOT NULL,
  `value` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_4`
--

LOCK TABLES `question_4` WRITE;
/*!40000 ALTER TABLE `question_4` DISABLE KEYS */;
INSERT INTO `question_4` VALUES (23,10),(24,10),(25,10),(26,5),(27,1),(28,8),(29,1),(30,1),(31,1),(32,1),(33,1),(34,5),(35,10),(36,9),(37,8),(38,8),(39,6),(40,1),(41,7),(42,4),(43,8);
/*!40000 ALTER TABLE `question_4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `value` varchar(2000) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `sum` int(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route`
--

LOCK TABLES `route` WRITE;
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
INSERT INTO `route` VALUES (23,'127.0.0.1:9000/admiqkwe;lwkq\';e','2016-04-18','11123',NULL),(24,'127.0.0.1:9000/admiqkwe;lw88%5Ckq\';e','2016-04-18','123213',NULL),(25,'127.0.0.1:9000/admiqkwe;lw88%5Cksq\';e','2016-04-18','4564',NULL),(26,'127.0.0.1:9000/admiqkwe;lw88%5Cksqsadadade','2016-04-18',NULL,NULL),(27,'127.0.0.1:9000/admiqkwqwewqeqwe','2016-04-18',NULL,8),(28,'127.0.0.1:9000/admiqkwqwewqeqwe7','2016-04-18','555',64),(29,'127.0.0.1:9000/admiqkwqwewqeqwe7','2016-04-18','555',44),(30,'127.0.0.1:9000/admiqkwqwewqeqwe7','2016-04-18','555',44),(31,'127.0.0.1:9000/admiqkwqwewqeqwe7','2016-04-18','555',44),(32,'127.0.0.1:9000/admiqkwqwewqeqwe7','2016-04-18','555',44),(33,'127.0.0.1:9000/admiqkwqwewqeqwe7','2016-04-18','555',44),(34,'127.0.0.1:9000/admiqkwe;lwkq\';e','2016-04-18','qwe',38),(35,'127.0.0.1:9000/admiqkwe;lwkq\';e','2016-04-18','11123',51),(36,'127.0.0.1:9000/admiqkwe5555554;lwkq\';e','2016-04-18','OLOLOLOLOLO',41),(37,'127.0.0.1:9000/;alsndl;kasnd','2016-04-19','454612',45),(38,'127.0.0.1:9000/pohylkhghmvmgcgchchgv','2016-04-19','qwe',36),(39,'127.0.0.1:9000/pohylkhghmvmgcgchchgv','2016-04-19','qwe',52),(40,'127.0.0.1:9000/pohylkhsghmvmgcgchchgv','2016-04-19','qwertt',8),(41,'127.0.0.1:9000/pohylkhsghmvmgcgchchgv','2016-04-19','qwertt',43),(42,'127.0.0.1:9000/admiqkwe5555554;lwkq\';e','2016-04-19','OLOLOLOLOLO',24),(43,'127.0.0.1:9000/admiqkwe5555554;lwkq\';e','2016-04-20','OLOLOLOLOLO',49);
/*!40000 ALTER TABLE `route` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-12 16:09:49
