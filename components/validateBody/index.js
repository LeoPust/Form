module.exports = function(body){
    var arrObj = {name:[
        'question_1_a',
        'question_1_b',
        'question_1_c',
        'question_2_a',
        'question_2_b',
        'question_2_c',
        'question_3',
        'question_4'
    ],value:[
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ]};
    for(var i in body){
        if(arrObj.name.indexOf(i) != -1){
            arrObj.value[arrObj.name.indexOf(i)] = body[i];
        }
    }
    if(arrObj.value.indexOf(0) == -1){
        return true;
    }else{
        return false;
    }
};