$(document).ready(function () {
    var arrObj = {name:[
        'question_1_a',
        'question_1_b',
        'question_1_c',
        'question_2_a',
        'question_2_b',
        'question_2_c',
        'question_3',
        'question_4'
    ],value:[
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ]};

    function BtnDisabled(){
        arrObj.value[arrObj.name.indexOf($(this).attr("name"))] = $(this).attr("value");
        if(arrObj.value.indexOf(0) == -1){
            $("button").removeAttr("disabled");
        }
    }

    $("input[name='question_1_a']").click(BtnDisabled);
    $("input[name='question_1_b']").click(BtnDisabled);
    $("input[name='question_1_c']").click(BtnDisabled);
    $("input[name='question_2_a']").click(BtnDisabled);
    $("input[name='question_2_b']").click(BtnDisabled);
    $("input[name='question_2_c']").click(BtnDisabled);
    $("input[name='question_3']").click(BtnDisabled);
    $("input[name='question_4']").click(BtnDisabled);

    $("button").click(function(){
        if(arrObj.value.indexOf(0) != -1){
            alert("You have empty fields");
            return false;
        }
    });
});