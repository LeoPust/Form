var Components = require("../index");

module.exports = function(req,callback){
  switch (true){
      case /^\/$/.test(req.url):
          callback({status:200,render:'index'});
          break;
      case /^\/(upd)$/.test(req.url):
          Components("UpdateUser")(req,callback);
          break;
      case /^\/(admin)\/\?\w+=\w+(&\w+=\w+)*/.test(req.url):
          Components("GetAllFeedBack")(req,callback);
          break;
      case /^\/*/.test(req.url):
          Components("AddFeedBack")(req,callback);
          break;
  }
};