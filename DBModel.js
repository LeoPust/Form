var objModels = {
    route:{
        id:     Number,
        value:  String,
        date:   String,
        name:   String,
        sum:    Number
    },
    question_1:{
        routeId:    Number,
        q_a:        Number,
        q_b:        Number,
        q_c:        Number
    },
    question_2:{
        routeId:    Number,
        q_a:        Number,
        q_b:        Number,
        q_c:        Number
    },
    question_3:{
        routeId:Number,
        value:Number
    },
    question_4:{
        routeId:Number,
        value:Number
    }
};

module.exports.objModels = objModels;

module.exports = function(db) {
    return {
        route : db.define("route",objModels.route),
        question_1   : db.define("question_1",objModels.question_1),
        question_2   : db.define("question_2",objModels.question_2),
        question_3   : db.define("question_3",objModels.question_3),
        question_4   : db.define("question_4",objModels.question_4)
    }
}
