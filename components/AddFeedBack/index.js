var moment = require('moment'),
    async = require('async');
moment().locale('ru');
var Componentns = require("../index");

module.exports = function(req,callback){
    switch (req.method){
        case "GET":
            callback({status:200,render:'index',body:{url:req.headers.host+req.url}});
            break;
        case "POST":
            Componentns("validateBody")(req.body) ?
            req.models.route.find({value:req.headers.host+req.url},function(er,route){
                var n = null;
                if (route.length > 0){
                    n = route[0].name;
                }
                req.models.route.create({
                        value:req.headers.host+req.url,
                        date:moment().format(),
                        name:n,
                        sum:Componentns("allSum")(req.body)
                    },
                    function(err,items){
                        !err ?
                            async.waterfall([
                                function(call){
                                    req.models.question_1.create({
                                        routeId:items.id,
                                        q_a:parseInt(req.body.question_1_a),
                                        q_b:parseInt(req.body.question_1_b),
                                        q_c:parseInt(req.body.question_1_c)
                                    },function(err){
                                        err ? call(null,false)
                                            :call(null,true);
                                    });
                                },
                                function(data,call){
                                    data ?
                                        req.models.question_2.create({
                                            routeId:items.id,
                                            q_a:parseInt(req.body.question_2_a),
                                            q_b:parseInt(req.body.question_2_b),
                                            q_c:parseInt(req.body.question_2_c)
                                        },function(err){
                                            err ? call(null,false)
                                                :call(null,true);
                                        })
                                        : call(null,false);
                                },
                                function(data,call){
                                    data ?
                                        req.models.question_3.create({
                                            routeId:items.id,
                                            value:parseInt(req.body.question_3)
                                        },function(err){
                                            err ? call(null,false)
                                                :call(null,true);
                                        })
                                        : call(null,false);
                                },
                                function(data,call){
                                    data ?
                                        req.models.question_4.create({
                                            routeId:items.id,
                                            value:parseInt(req.body.question_4)
                                        },function(err){
                                            err ? call(null,false)
                                                :call(null,true);
                                        })
                                        : call(null,false);
                                }
                            ],function(err,result){
                                result ?
                                    callback({status:200})
                                    : callback({status:403});
                            })
                            : callback({status:403});
                    })
            })
                : callback({status:403});
            break;
    }
};