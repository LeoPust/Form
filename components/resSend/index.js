module.exports = function(res,results){
    if(!results.error){
        if(results.redirect){
            res.redirect(results.redirect);
        }else {
            if (results.render) {
                if (results.body) {
                    res.status(results.status).render(results.render, results.body);
                    return;
                } else {
                    res.status(results.status).render(results.render);
                    return;
                }
            } else if (results.body) {
                res.status(results.status).json(results.body);
                return;
            } else {
                res.sendStatus(results.status);
                return;
            }
        }
    }else{
        res.sendStatus(results.error);
        return;
    }
};